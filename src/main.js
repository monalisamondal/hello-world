window.$ = window.jQuery = require('jquery');

import 'jquery';
import Vue from 'vue';

import App from './App.vue';
import router from './router';
import VueApexCharts from 'vue-apexcharts';
import VuePeity from 'vue-peity'


Vue.use(VueApexCharts)
Vue.use(VuePeity)

Vue.component('apexchart', VueApexCharts)
Vue.component('peity', VuePeity)


require("./assets/lib/jquery/jquery.min");
require("./assets/lib/bootstrap/js/bootstrap.bundle.min");
require("./assets/lib/ionicons/ionicons");
require("./assets/lib/jquery.flot/jquery.flot");
require("./assets/lib/chart.js/Chart.bundle.min");
require("./assets/lib/peity/jquery.peity.min");
require("./assets/js/azia");
require("./assets/js/chart.chartjs");
require("./assets/js/chart.flot");
require("./assets/js/chart.flot.sampledata");
require("./assets/js/chart.morris");
require("./assets/js/chart.peity");
require("./assets/js/chart.sparkline");
require("./assets/js/cookie");
require("./assets/js/dashboard.sampledata");
require("./assets/js/jquery.vmap.sampledata");
require("./assets/js/map.apple");
require("./assets/js/map.bluewater");
require("./assets/js/map.mapbox");
require("./assets/js/map.shiftworker");
require('./assets/lib/jquery-ui/ui/widgets/datepicker.js');

require("./assets/lib/fontawesome-free/css/all.min.css");
require('./assets/lib/ionicons/css/ionicons.min.css');
require("./assets/lib/typicons.font/typicons.css");
require("./assets/lib/flag-icon-css/css/flag-icon.min.css");
require("./assets/css/azia.css");
require("./assets/css/azia.min.css");

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
