import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);
const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			component: resolve => require(["@/components/protected/Workspace.vue"], resolve),
			children: [
				{
					path: "",
					name: "dashboard",
					component: resolve => require(["@/components/protected/Dashboard/Dashboard.vue"], resolve)
				},
				{
					path: "dashboard2",
					name: "dashboard",
					component: resolve => require(["@/components/protected/Dashboard2/Dashboard2.vue"], resolve)
				},
				{
					path: "dashboard3",
					name: "dashboard",
					component: resolve => require(["@/components/protected/Dashboard3/Dashboard3.vue"], resolve)
				},
				{
					path: "dashboard4",
					name: "dashboard",
					component: resolve => require(["@/components/protected/Dashboard4/Dashboard4.vue"], resolve)
				},
			]
		},
	]
});

export default router;
